<?php
use Laratube\Http\Controllers\UploadVideoController;
use Laratube\Http\Controllers\VideoController;
use Laratube\Http\Controllers\VoteController;
use Laratube\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('channels', 'ChannelController');

Route::get('comments/{comment}/replies', [CommentController::class, 'show']);
Route::get('videos/{video}/comments', [CommentController::class, 'index']);

Route::get('videos/{video}', [VideoController::class, 'show'])->name('videos.show');
Route::put('videos/{video}', [VideoController::class, 'updateViews']);



Route::group(['middleware' => ['auth']], function () {
    Route::resource('channels/{channel}/subscriptions', 'SubscriptionController')->only(['store','destroy']);

    Route::post('votes/{entityId}/{type}', [VoteController::class, 'vote']);

    Route::get('channels/{channel}/videos', [UploadVideoController::class, 'index'])->name('channel.upload');
    Route::post('channels/{channel}/videos', [UploadVideoController::class, 'store']);    

    Route::put('videos/{video}/video', [VideoController::class, 'update'])->name('videos.update');

    Route::post('comments/{video}', [CommentController::class, 'store']);
});

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if($video->editable())
                <form action="{{ route('videos.update', $video->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    @endif

                    <div class="card-header">{{$video->title}}</div>

                    <div class="card-body">
                        <video-js id='video' class='vjs-default-skin' controls preload='auto' width='640' height='264'>
                            <source src='{{asset(Storage::url("videos/{$video->id}/{$video->id}.m3u8"))}}'
                                type='application/x-mpegURL'>
                            <p class='vjs-no-js'>
                                To view this video please enable JavaScript, and consider upgrading to a web browser
                                that
                                <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5
                                    video</a>
                            </p>
                        </video-js>

                        <div class="row mt-3">
                            <div class="col-8">
                                <h5 class="text-truncate">
                                    @if($video->editable())
                                    <input type="text" class="form-control" value="{{$video->title}}" name="title">
                                    @else
                                    {{$video->title}}
                                    @endif
                                </h5>
                                <h6><i class="fa fa-eye mr-2" aria-hidden="true"></i>{{$video->views}} views</h6>
                            </div>
                            <div class="col-4 text-right">
                                <votes :default_votes='{{$video->votes}}' entity_id='{{$video->id}}'
                                    entity_owner='{{$video->channel->user_id}}'></votes>
                            </div>
                        </div>

                        <div class="border-bottom my-2"></div>

                        <div class="my-4">
                            @if($video->editable())
                            <textarea class="form-control" name="description" id="description"
                                rows="3">{{$video->description}}</textarea>
                            <div class="text-right">
                                <button class="btn btn-info mt-2" type="submit">Update Channel</button>
                            </div>
                            @else
                            {{$video->description}}
                            @endif
                        </div>
                        <div class="border-bottom my-2"></div>

                        <div class="d-flex justify-content-between mt-4 align-items-center">
                            <div class="media d-flex align-items-center">
                                <img src="{{$video->channel->image()}}" alt="" width="50" height="50"
                                    class="rounded-circle border">
                                <div class="media-body ml-2">
                                    <h5 class="my-0">{{$video->channel->name }}</h5>
                                    <span class="small">Published on {{$video->created_at->toFormattedDateString()}}
                                    </span>
                                </div>
                            </div>

                            <subscribe-button :initial-subscriptions="{{ $video->channel->subscriptions }}"
                                :channel="{{ $video->channel }}" />

                        </div>

                    </div>
                    @if($video->editable())
                </form>
                @endif
            </div>

        <comments :video="{{ $video }}"></comments>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="https://vjs.zencdn.net/7.6.0/video-js.css" rel="stylesheet">
@endsection

@section('scripts')
<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src='https://vjs.zencdn.net/7.6.0/video.js'></script>
<script>
    var player = videojs('video')

    var viewLog = false
    player.on('timeupdate', function(){
        var perPlay = Math.ceil(player.currentTime()/player.duration()*100)

        if(perPlay>5 && !viewLog){
            axios.put('/videos/{{$video->id}}')

            viewLog = true;
        }
    })
</script>
@endsection
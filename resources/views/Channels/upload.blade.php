@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <channel-uploads :channel="{{ $channel }}" inline-template>
            <div class="col-md-8">
                
                <div class="card p-3 d-flex justify-content-center align-items-center" v-if="!selected">
                    <svg onclick="document.getElementById('video-files').click()" width="80px" height="80px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 461.001 461.001"
                        style="enable-background:new 0 0 461.001 461.001;" xml:space="preserve">
                        <path style="fill:#F61C0D;" d="M365.257,67.393H95.744C42.866,67.393,0,110.259,0,163.137v134.728
               c0,52.878,42.866,95.744,95.744,95.744h269.513c52.878,0,95.744-42.866,95.744-95.744V163.137
               C461.001,110.259,418.135,67.393,365.257,67.393z M300.506,237.056l-126.06,60.123c-3.359,1.602-7.239-0.847-7.239-4.568V168.607
               c0-3.774,3.982-6.22,7.348-4.514l126.06,63.881C304.363,229.873,304.298,235.248,300.506,237.056z" />
                    </svg>

                    <input type="file" multiple ref="videos" id="video-files" class="d-none" @change="upload">
                    <p class="text-center">Upload Videos</p>
                </div>

                <div class="card p-3" v-else>

                    <div class="my-4" v-for="video in videos">
                        <div class="progress mb-3">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar"
                                :style="{width: `${video.percentage || progress[video.name]}%`}" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                @{{video.percentage? video.percentage===100?'Video Proccessing Completed' : 'Processing' : 'Uploading' }}</div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <div v-if="!video.thumbnail" class="d-flex justify-content-center align-items-center" style="height:180px;color:white;font-size:18px;background-color:#ccc;">
                                    Loading thumbnail ...
                                </div>

                                <img v-else :src="video.thumbnail" class="w-100" alt="">
                            </div>

                            <div class="col-md-8">

                                <a v-if="video.percentage && video.percentage === 100" target="_blank" :href="`/videos/${video.id}`">
                                        @{{ video.title }}
                                </a>

                                <h5 v-else class="py-2">
                                    @{{ video.title || video.name}}
                                </h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </channel-uploads>
    </div>
</div>
@endsection
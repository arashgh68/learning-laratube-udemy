<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Laratube\Model;
use Faker\Generator as Faker;
use Laratube\Subscription;
use Laratube\Channel;
use Laratube\User;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'channel_id' => function(){
            return factory(Channel::class)->create()->id;
        },
        'user_id' => function(){
            return factory(User::class)->create()->id;
        }
    ];
});
